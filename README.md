# KPKT Scraper

This scraper retrieve developer and project data from http://ehome.kpkt.gov.my/index.php/pages/view/172.

# Setup

Requirements

* Python 3
* [requests](https://pypi.org/project/requests/) library
* [Beautiful Soup 4](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)

Recommend install libraries using [PIP](https://packaging.python.org/tutorials/installing-packages/)

```bash
pip3 install requests
pip3 install beautifulsoup4
```

# Running Scraper

```bash
./kpkt_scraper.py
```

By default, it will output data into *kpkt_data.json* in current directory. Optionally, output file path can be specified in command line argument.

```bash
./kpkt_scraper.py /path/to/output_file.json
```

In both cases, file will be created if it does not exist yet. If there is existing file, it will be **overwritten**.

# Output Data File

1 developer per line. Each line is a developer object.

Example:

```json
{
  "name": "ABID CEMPAKA SDN BHD",
  "ssm": "328645-A",
  "projects": {
    "1": {
      "name": "TAMAN SERULING JAYA",
      "license_num": "10823-1/10-2011/1020",
      "license_expiry": "16/10/2011",
      "permit_num": "10823-1/2618/2010(12)",
      "permit_expiry": "30/12/2010",
      "builds": [
        {
          "num_units": 118,
          "type": "\r\nRUMAH TERES",
          "category": "\r\nHARGA SEDERHANA RENDAH",
          "num_floors": 1,
          "min_price": 70000,
          "max_price": 180397
        }
      ]
    },
    "2": {
      "name": "TAMAN SERULING JAYA",
      "license_num": "10823-2/09-2014/1073",
      "license_expiry": "11/09/2014",
      "permit_num": "10823-2/1830/2012(09)",
      "permit_expiry": "12/09/2012",
      "builds": [
        {
          "num_units": 476,
          "type": "\r\nRUMAH TERES",
          "category": "\r\nHARGA SEDERHANA",
          "num_floors": 1,
          "min_price": 98500,
          "max_price": 154836.02
        }
      ]
    }
  }
}
```