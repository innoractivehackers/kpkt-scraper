#!/usr/bin/env python3

"""
Instructions
- Please use pip3 to install dependency libs: requests, beautifulsoup4
- Run this file from command line, it will output data into 'kpkt_data.json' file. 1 developer JSON object per line.
"""

import json
import re
import sys

import requests
from bs4 import BeautifulSoup


"""
Extract developer code from URI. URI example: "maklumatPemaju.cfm?pmju_kod=7568"
"""
def developer_code(uri):
    return uri[28:]

"""
Add built infomation to a project.
List all builds: http://idaman2.kpkt.gov.my:8888/idv5/98_eHome/maklumatProjek.cfm?pmju_kod=2277&proj_kod_Fasa=5
"""
def add_project_builds(developer_code, phase, project):
    url = 'http://idaman2.kpkt.gov.my:8888/idv5/98_eHome/maklumatProjek.cfm?pmju_kod={0}&proj_kod_Fasa={1}'.format(developer_code, phase)
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')

    table = soup.select('table.MainContent')[3]
    for row in table.tbody.find_all('tr'):
        cells = row.find_all('td')
        project['builds'].append({
            'num_units': int(cells[1].string),
            'type': cells[2].string,
            'category': cells[3].string,
            'num_floors': float(cells[4].string),
            'min_price': float(cells[5].string.replace(',', '')),
            'max_price': float(cells[6].string.replace(',', '')),
        })

    #print(project['builds'])

"""
Add projects to developer dict by scraping project info.
List all projects: http://idaman2.kpkt.gov.my:8888/idv5/98_eHome/maklumatPemaju.cfm?pmju_kod=2277
"""
def add_projects(developer_code, developer):
    """
    - get license dates
    - get unit info
    """
    response = requests.get('http://idaman2.kpkt.gov.my:8888/idv5/98_eHome/maklumatPemaju.cfm?pmju_kod=' + developer_code)
    soup = BeautifulSoup(response.text, 'html.parser')
    
    # 1st table is developer info, 2nd is project listing
    table = soup.select('table.MainContent')[1]
    for row in table.tbody.find_all('tr'):
        cells = row.find_all('td')
        phase = cells[2].string

        """
        Example: 
        2277-5/02-2017/0106(L)
        (10/02/2017)
        2277-5/02-2017/0106(P)
        (10/02/2017) 
        """
        info = cells[4].contents # License and permit info.
        
        if phase not in developer['projects']:
            developer['projects'][phase] = {
                'name': cells[3].a.string,
                'license_num': info[0].strip(),
                'license_expiry': re.search(r'\((.*)\)', info[2].strip()).group(1),
                'permit_num': info[4].strip(),
                'permit_expiry': re.search(r'\((.*)\)', info[6].strip()).group(1),
                'builds': [],
            }
        
        print('Adding project({0}) phase({1})'.format(developer['projects'][phase]['name'], phase))
        add_project_builds(developer_code, phase, developer['projects'][phase])

"""
Search based on SSM number.
Output to `file`. Each line is a JSON object describing a developer and all its projects.
"""
def search_by_ssm(ssm, processed_developers, file):
    data = {'cariPemaju': 'Cari', 'pmju_ROC': ssm}
    response = requests.post('http://idaman2.kpkt.gov.my:8888/idv5/98_eHome/carianPemaju.cfm', data)
    soup = BeautifulSoup(response.text, 'html.parser')
    
    # 1st table is search form, 2nd is result
    table = soup.select('table.MainContent')[1]
    for row in table.tbody.find_all('tr'):
        cells = row.find_all('td')
        code = developer_code(cells[4].a.get('href'))

        if code in processed_developers:
            continue
        
        processed_developers.append(code)
        
        developer = {
            'name': cells[4].a.string,
            'ssm': cells[5].string,
            'projects': {},
        }
        
        print('Adding developer "{0}"[{1}]'.format(developer['name'], code))
        add_projects(code, developer)

        file.write(json.dumps(developer) + '\n')
        file.flush()

    # Structure: arr[developer][project][phase]

"""
Determine output file from command line.
First argument in command line is file path.
Default: kpkt_data.json
"""
def output_file():
    if len(sys.argv) == 1:
        return 'kpkt_data.json'
    else:
        return sys.argv[1]

def main():
    # Prevent same developers from being processed twice
    processed_developers = []

    with open(output_file(), 'w') as file:
        # SSM suffix from A to Z
        for ssm in range(65,91):
            print('Processing SSM ending with ' + chr(ssm))
            search_by_ssm(chr(ssm), processed_developers, file)

if __name__ == "__main__":
    main()
